import matplotlib.pyplot as plt
import subprocess
import os
from screeninfo import get_monitors

# autosize font
try:
    m = get_monitors()
    m.sort(key=lambda x: -x.width_mm)
    font = m[0].width_mm * m[0].width_mm // 25000
    hspc = 4 / (font * font)
    wspc = hspc
    bt = 2 * wspc / 3
except Exception as ex:
    print("Warning: display not founded.")
    font, hspc, wspc, bt = 8, 0.125, 0.25, 0.167

if not os.path.isfile('./ex1/0'):
    print("Create test file '0' in the directory './ex1/'."
          + "\nFile './ex1/0' not founded")
    exit()
bashCommand = ("python3" if os.name != "nt" else "python") + " main.py ./ex1/0 -t "
mods = ['-e 2', '-e 3', '-e 5', '-e 4', '-e 6', '-e 1']  # , '-a 2', '-a 3']
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
output, error = process.communicate()
if str(output).find("Error") != -1:
    print("Error in the program 'python3 main.py ./ex1/0 -t ', here is output:\n")
    [print(x) for x in str(output)[2:len(str(output)) - 1].split("\\n")]
    exit()

labels = ['Manhattan', 'Linear', 'Corner', 'Hamming', 'Hamm+', 'Euclid']  # , 'Alg Uniform', 'Alg Greedy']
sizes = []
tm_cs = []
tms = []
moves = []
for x, l in zip(mods, labels):
    process = subprocess.Popen((bashCommand + x).split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    size = float(str(output).split("Size complexity:")[1].split("\\n")[0]) / 1000
    tm_c = float(str(output).split("Time complexity:")[1].split("\\n")[0]) / 1000
    tm = float(str(output).split("Process time:")[1].split("\\n")[0])
    mov = float(str(output).split("Moves:")[1].split("\\")[0])

    sizes.append(size)
    tm_cs.append(tm_c)
    tms.append(tm)
    moves.append(mov)

    print("Mode " + l + ":")
    print("\tComplexity Size", size)
    print("\tComplexity Time", tm_c)
    print("\tTime", tm, "mili seconds")
    print("\tMoves", mov)

fig, bar = plt.subplots(nrows=2, ncols=2)
fig.subplots_adjust(bottom=bt, hspace=hspc, wspace=wspc)
bars = bar.flatten().tolist()
data = [sizes, tm_cs, moves, tms]

for i, l in enumerate(["Complexity Size, 10^3", "Complexity Time, 10^3", "Moves", "Time, 10^(-6)"]):
    bars[i].bar(labels, data[i])
    bars[i].tick_params(axis='x', size=font, rotation=40)
    bars[i].set_ylabel(l)
bars[0].axes.xaxis.set_ticklabels([])
bars[1].axes.xaxis.set_ticklabels([])

plt.show()
