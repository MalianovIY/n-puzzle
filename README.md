# N-puzzle

School 21 (Ecole 42) students project, realise n-puzzle solver.

Program `main.py` takes the following flags as parameters:
```
    puzzle                                  file with N, puzzle
    -h, --help                              show this help message and exit
    --time, -t                              shows the used time
    --heuristic HEURISTIC, -e HEURISTIC     choose heuristics
    --algo ALGO, -a ALGO                    choose algorithms
    --stats, -s                             print the steps to solution
```

Added heuristic functions: 
1. Euclidean distance 
2. Manhattan distance
3. Linear conflict 
4. Hamming distance 
5. Corner tiles
6. Humming Plus (hit current row or column number and final)


Implement algorithms:
1. A-star 
2. Uniform 
3. Greedy search

### Something about heuristic
* [in Russian](https://resetius.ru/programs/15.html)
* [in English](https://algorithmsinsight.wordpress.com/graph-theory-2/a-star-in-general/implementing-a-star-to-solve-n-puzzle/)

### Additionally

This repo has comparsion.py, program where I commpare different heuristic functions 
by time, size complexity, moves and processed times, and shows different on graph

Also, the repo contains generator.py - simple generator of n-puzzle.

##### Graph comparsion
![Screenshot_0](https://gitlab.com/MalianovIY/n-puzzle/-/raw/master/docs/0.png)

### Subject
[Subject](https://cdn.intra.42.fr/pdf/pdf/17244/en.subject.pdf)
