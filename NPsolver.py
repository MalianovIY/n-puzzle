import math as m
from copy import deepcopy as cp
import heapq as hp
import time as tm


class NPSolver:
    def __init__(self, size, board, final, heu, algo, time, stat):
        self.size = size
        self.board = board
        self.final = final
        self.heu = heu
        self.uniform = (algo == 2)
        self.greedy = (algo == 3)
        self.time = tm.time() if time else 0
        self.stat = stat
        self.f_dict = {}
        if self.heu == 3:
            for i in range(self.size * self.size):
                self.f_dict[self.final[i]] = i

    def cost(self, br):
        c = 0
        for i in range(1, self.size * self.size):
            cur = br.index(i)
            fin = self.final.index(i)
            xc, yc = cur // self.size, cur % self.size
            xf, yf = fin // self.size, fin % self.size
            # euclid, manhattan, linear conflict, hamming, corner tiles, humming+
            if self.heu == 1:
                c += m.sqrt((xc - xf) * (xc - xf) + (yc - yf) * (yc - yf))
            elif self.heu == 2 or self.heu == 3 or self.heu == 5:
                c += abs(xc - xf) + abs(yc - yf)
            elif self.heu == 3:
                for j in range(self.size):
                    ix = self.f_dict[j * self.size + yc]
                    iy = self.f_dict[xc * self.size + j]
                    xix, yix = ix // self.size, ix % self.size
                    xiy, yiy = iy // self.size, iy % self.size
                    c += (int(xiy == xc and (yiy < yc < j or yiy > yc > j)) +
                          int(yix == yc and (xix < xc < j or xix > xc > j)))
            elif self.heu == 4:
                c += int(cur != fin)
            elif self.heu == 5:
                t = [[[0, self.size - 2], [1, self.size - 1]], [[self.size - 1, 1], [self.size - 2, 0]],
                     [[self.size - 1, self.size - 2], [self.size - 2, self.size - 1]], [[0, 1], [1, 0]]]
                t1 = [[0, self.size - 1], [self.size - 1, 0], [self.size - 1, self.size - 1], [0, 0]]
                if any([cor[0] == xc != xf and cor[1] == yc != yf
                        and self.final[bl[0][0] * self.size + bl[0][1]] == br[bl[0][0] * self.size + bl[0][1]]
                        and self.final[bl[1][0] * self.size + bl[1][1]] == br[bl[1][0] * self.size + bl[1][1]]
                        for bl, cor in zip(t, t1)]):
                    c += 2
            elif self.heu == 6:
                c += int(xc != xf) + int(yc != yf)
        return c

    def neighbor(self, br):
        out = []
        tmp = list(br)
        i = br.index(0)
        x, y = i // self.size, i % self.size
        if x > 0:
            t = cp(tmp)
            t[x * self.size + y] = t[(x - 1) * self.size + y]
            t[(x - 1) * self.size + y] = 0
            out.append(tuple(t))
        if y > 0:
            t = cp(tmp)
            t[x * self.size + y] = t[x * self.size + y - 1]
            t[x * self.size + y - 1] = 0
            out.append(tuple(t))
        if x < self.size - 1:
            t = cp(tmp)
            t[x * self.size + y] = t[(x + 1) * self.size + y]
            t[(x + 1) * self.size + y] = 0
            out.append(tuple(t))
        if y < self.size - 1:
            t = cp(tmp)
            t[x * self.size + y] = t[x * self.size + y + 1]
            t[x * self.size + y + 1] = 0
            out.append(tuple(t))
        return out

    def print_res(self, tm_compl, size_compl, puz):
        print("Time complexity: ", tm_compl, "\nSize complexity: ", size_compl)
        if self.time:
            print("Process time: ", round(tm.time() - self.time, 5))
        i = 0
        t = puz
        while t is not None:
            if self.stat:
                for j in range(self.size):
                    print(" ".join([str(x) for x in t[3][j * self.size:(j + 1) * self.size]]))
                print()
            i += 1
            t = t[4]
        print("Moves: ", i)

    def solve(self):
        if self.uniform:
            c = 0
        else:
            c = self.cost(self.board)
        op = []
        hp.heappush(op, (
            c,  # full cost - heuristic and cost to start
            c,  # heuristic cost
            0,  # cost to start
            self.board,  # board
            None  # parent
        ))
        op2 = {op[0][2]: {
            "f": c,
            "h": c,
            "g": 0,
            "puz": self.board,
            "par": None
        }}
        cl = {}
        size_compl = 1
        tm_compl = 1
        end = False
        while not end and len(op):
            temp = hp.heappop(op)
            if temp[3] != self.final:
                size_compl += 1
                cl[temp[3]] = temp[0]
                for i in self.neighbor(temp[3]):
                    if i in cl:
                        continue
                    if self.uniform:
                        heu = 0
                    else:
                        heu = self.cost(i)
                    t_cost = temp[2] + 1
                    f_cost = heu
                    if not self.greedy:
                        f_cost += t_cost
                    if i not in op:
                        hp.heappush(op, (f_cost, heu, t_cost, i, temp))
                        size_compl += 1
                        tm_compl += 1
                        if i not in op2:
                            size_compl += 1
                        op2[i] = [(f_cost, heu, t_cost, i, temp)]
                    else:
                        if op2[i][0][0] < f_cost:
                            hp.heappush(op, (f_cost, heu, t_cost, i, temp))
                            op2[i].insert(0, (f_cost, heu, t_cost, i, temp))
                            size_compl += 1
            else:
                self.print_res(tm_compl, size_compl, temp)
                end = True
