import argparse
import numpy as np
from NPsolver import NPSolver


def final_state(sz):
    direction = 0
    i, j = 0, 0
    out = np.zeros((sz, sz), dtype=int)
    for k in range(1, sz * sz):
        out[i][j] = k
        if direction == 0 and (j == sz - 1 or out[i][j + 1] > 0):
            direction = 1
            i += 1
        elif direction == 0:
            j += 1
        elif direction == 1 and (i == sz - 1 or out[i + 1][j] > 0):
            direction = 2
            j -= 1
        elif direction == 1:
            i += 1
        elif direction == 2 and (j == 0 or out[i][j - 1] > 0):
            direction = 3
            i -= 1
        elif direction == 2:
            j -= 1
        elif direction == 3 and (i == 0 or out[i - 1][j] > 0):
            direction = 0
            j += 1
        elif direction == 3:
            i -= 1
    return out


def solvable(br, sz):
    f = final_state(sz).flatten()
    inv = 0
    for i in range(sz * sz - 1):
        for j in range(i + 1, sz * sz):
            before = br[i]
            after = br[j]
            if np.where(f == before) > np.where(f == after):
                inv += 1
    i_0 = np.where(br == 0)[0][0]
    j_0 = np.where(f == 0)[0][0]
    return inv % 2 == ((i_0 % sz - j_0 % sz) + (i_0 // sz - j_0 // sz)) % 2


def parse(name):
    sz = -1
    br = []
    try:
        with open(name) as f:
            for i, line in enumerate(f):
                a = line.split() if line.find('#') == -1 else line[:line.find('#')].split()
                try:
                    a = [int(x) for x in a]
                except ValueError:
                    print("Error, board contain not integer value")
                    exit(1)
                if len(a) == 0:
                    continue
                elif len(a) == 1 and sz == -1:
                    if a[0] < 3:
                        print("Board size is invalid, size value", a[0], "< 3")
                        exit()
                    sz = a[0]
                else:
                    if len(a) != sz:
                        print("Board size does not match for line ", i)
                        exit(1)
                    br.append(a)
    except Exception as ex0:
        print("Invalid file: ", ex0.args)
        exit(1)
    s = set()
    [s.update(i) for i in br]
    if max(s) != sz * sz - 1 or min(s) != 0 or len(s) != sz * sz:
        print("The board is invalid, must contain number from 0 to ", sz * sz - 1)
        exit(1)
    return br, sz


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("puzzle", help="File with N, puzzle")
    parser.add_argument("--time", "-t", default=False, action="store_true",
                        help="shows the used time")
    parser.add_argument("--heuristic", "-e", type=int, default=2,
                        help="Heuristic functions: 1 for Euclidean distance, "
                             + "2 for Manhattan distance, 3 for Linear conflict, "
                             + "4 for Hamming distance, 5 for Corner tiles, "
                             + "6 for Humming Plus (I forgot a real name)")
    parser.add_argument("--algo", "-a", type=int, default=1,
                        help="Algorithm: 1 for  A*, 2 for uniform, "
                             + "3 for greedy search")
    parser.add_argument("--stats", "-s", default=False, action="store_true",
                        help="print the steps to solution")
    args = parser.parse_args()
    if args.heuristic not in [1, 2, 3, 4, 5, 6]:
        print("Please select the correct Heuristic.")
        exit(1)
    if args.algo not in [1, 2, 3]:
        print("Please select the correct Algorithm.")
        exit(1)
    try:
        board, size = parse(args.puzzle)
        if solvable(np.array(board).flatten(), size):
            solve = NPSolver(
                size,
                tuple(np.array(board).flatten()),
                tuple(final_state(size).flatten()),
                args.heuristic,
                args.algo,
                args.time,
                args.stats
            )
            solve.solve()
        else:
            print("This n-puzzle is unsolvable.")
    except Exception as ex:
        print("I don't know what's happens!", ex.args)
        exit(1)
